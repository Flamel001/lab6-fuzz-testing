import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");


describe('Bonus system tests', () => {
    let app;
    console.log("Tests started");
    const standard = "Standard",
    standardAmount = 0.05,
    premium = "Premium", 
    premiumAmount = 0.1,
    dimond = "Diamond",
    dimondAmount = 0.2,
    smallBonus = 1,
    midBonus = 1.5,
    bigBonus = 2,
    hugeBonus = 2.5;
    test('invalid program',  (done) => {
        let a = 'js one love';
        let b = -999;

        expect(calculateBonuses(a,b)).toEqual(0);
        done();
    });

    test('Negative amount standard',  (done) => {
        let a = standard;
        let b = -1;

        expect(calculateBonuses(a,b)).toEqual(standardAmount * smallBonus);
        done();
    });

    test('9999 amount premium',  (done) => {
        let a = premium;
        let b = 9999;

        expect(calculateBonuses(a,b)).toEqual(premiumAmount * smallBonus);
        done();
    });

    test('10000 amount dimond',  (done) => {
        let a = dimond;
        let b = 10000;

        expect(calculateBonuses(a,b)).toEqual(dimondAmount * midBonus);
        done();
    });

    test('49999 amount standard',  (done) => {
        let a = standard;
        let b = 49999;

        expect(calculateBonuses(a,b)).toEqual(standardAmount * midBonus);
        done();
    });

    test('50000 amount premium',  (done) => {
        let a = premium;
        let b = 50000;

        expect(calculateBonuses(a,b)).toEqual(premiumAmount * bigBonus);
        done();
    });

    test('99999 amount dimond',  (done) => {
        let a = dimond;
        let b = 99999;

        expect(calculateBonuses(a,b)).toEqual(dimondAmount * bigBonus);
        done();
    });

    test('100000 amount standard',  (done) => {
        let a = standard;
        let b = 100000;

        expect(calculateBonuses(a,b)).toEqual(standardAmount * hugeBonus);
        done();
    });

    test('100500 amount premium',  (done) => {
        let a = standard;
        let b = 100500;

        expect(calculateBonuses(a,b)).toEqual(standardAmount * hugeBonus);
        done();
    });
    console.log('Tests Finished');

});
